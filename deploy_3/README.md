# Configurando a Integração Contínua usando o Jenkins

Nesse deploy iremos adicionar um elemento a mais para nos auxiliar na integração contínua do código-fonte para acelerar o feedback a respeito da qualidade do código e se tudo está ok para ir para a próxima etapa do pipeline de implantação.

## Pré-requisitos

- Docker instalado
- Git instalado
- Ansible instalado

## Setup

Vamos precisar iniciar um novo servidor com o Jenkins que é uma ferramenta bastante conhecida e utilizada para suporte a integração contínua. Para iniciar o servidor vamos executar o comando abaixo:

- Primeiro vamos gerar uma imagem nova para o container Jenkins com alguns pacotes a mais que precisaremos para o gerar o build do projeto

> sudo docker build -t jenkins-devops .

- Depois da imagem gerada podemos iniciar um container novo usando a nova imagem.

> sudo docker run -idt -p 8080:8080 -p 50000:50000 --name cpf-api-ci jenkins-devops

Após o processo indicar que foi inicializado apresentando o ID do container execute o comando para ver o log do container e a senha de instalação gerada durante a inicialização do jenkins no container, você verá algo assim:

> sudo docker logs cpf-api-ci

```
Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:

5b2d64daaae3453c8383e0946cfd6f51

This may also be found at: /var/jenkins_home/secrets/initialAdminPassword
```

Esse `5b2d64daaae3453c8383e0946cfd6f51` é a senha gerada durante o instalação e configuração do jenkins no container, agora vamos para a configuração do Jenkins.

1.  No primeiro passo ele vai pedir para escolher se prefere instalar os plugins básicos sugeridos pelo Jenkins ou se prefere escolher, vamos escolher a opção para selecionarmos os plugins, e lá vamos adicionar o plugin do `NodeJS` e `Cobertura` (use a pesquisa para facilitar a configuração).
2.  Após a instalação dos plugins iniciais, serão solicitados os dados do usuário admin do Jenkins, para facilitar preencha tudo com `jenkins` e coloque no último campo o seu email.
3.  Ativar o plugin `Checkstyle`, acesse o menu `Manage Jenkins > Manager Plugins` e acesse a aba `Available`, depois utilize a pesquisa para encontrar o plugin `Checkstyle` e clique em `Install without restart`.
4.  Agora precisamos configurar o NodeJS que vamos usar no menu `Manage Jenkins > Global Tool Configuration`
5.  Na sessão NodeJS, adicione uma instalação e chame-a de `NodeJS` e escolha a versão `NodeJS 10.4.0`. No campo `Global npm packages to install` você vai adicionar os plugins `jasmine-node` e `istanbul`.
6.  Se todas as etapas anteriores passarem agora o Jenkins está pronto para iniciar o uso.

## Exercício 1

Vamos agora configurar o Jenkins para auxiliar no processo de build e teste. Siga os passos a seguir:

1.  Adicione um novo projeto em `New Job` no modo `Free-style`.
2.  Coloque o nome do projeto `cpf_api` e uma descrição.
3.  Marque a opção para descartar builds antigos e preencha o campo para manter no máximo `5` builds.
4.  Na sessão `Source Code Management` marque a opção `Git` e coloque no campo `Repository URL` o endereço `https` do repositório, as demais opções pode manter como está.
5.  Na sessão `Build Environment` marque a opção `Provide Node & npm bin/ folder to PATH` e escolha a instalação `NodeJS` que você configurou anteriormente.
6.  Na sessão `Build` você vai adicionar um novo `Execute shell` e adicionar os comandos para realizar o build e executar os testes exportando o relatório com o resultado:

```
npm install
npm test
```

9.  Salve o projeto e clique novamente em `Build Now`

Após o último passo você já tem o processo de integração contínua automatizado e dando feedback sobre os testes automatizados.

Agora vamos adicionar algumas informações para auxiliar os desenvolvedores com mais informações a respeito do qualidade do código-fonte.

10. Para configurar a exibição dos relatórios vamos especificar os relatórios na sessão `Post-Build Actions`

- para os relatórios da execução dos testes escolha a opção `Publish JUnit test result report` e adicione no campo para especificar o caminho do relatório `reports/TEST-cpfvalidator.xml`.

11. Vamos adicionar a checagem da padronização do código-fonte e as estatísticas de cobertura dos testes. Adicione no `Execute Shell` mais duas linhas.

```
npm run lint
npm run coverage
```

O Build ficará assim:

```
npm install
npm test
npm run lint
npm run coverage
```

- para adicionar os relatórios de padronização do código-fonte (checkstyle) escolha a opção `Publish Checkstyle analysis results` e adicione no campo para especificar o caminho do relatório `reports/eslint.xml`.

- para os relatórios de cobertura dos testes escolha a opção `Publish Cobertura Coverage Report` e adicione no campo para especificar o caminho do relatório `coverage/cobertura-coverage.xml`.

12. Salve o projeto e clique novamente em `Build Now`.

## Exercício 2

Vamos alterar o código adicionando um novo teste.

1.  Insira o código abaixo no arquivo `spec/units/cpfValidatorSpec.js` logo após o último cenário de teste (identificado pelo método `it`) no projeto `cpf_validator_api`:

```
it("wrong format", function() {
  const result = app.checkCorrectSize("123.123.123-30");

  expect(result).toBe(false);
});
```

2.  Após realizar a alteração você precisa submeter a sua alteração através do `commit` e `push`, execute os comandos a seguir:

> git commit spec/units/cpfValidatorSpec.js. -m 'adicionado um novo caso de teste'

> git push origin master

3.  Agora que o código foi atualizado acesse novamente o Jenkins (http://devops.prod:8080) e execute o build do projeto novamente

4.  Assim que finalizado o build confira os relatórios e veja se deu tudo certo!

5.  Agora para finalizar vamos implantar o build que foi integrado e validado executando o playbook novamente

> ansible-playbook -i hosts app.yml

6.  Finalizando o deploy verifique a api em `http://devops.prod/cpf_api/`

### Possível problema!!!

É possível que ocorra um problema durante o deploy, já existe um processo no pm2 com a mesma identificação então não é possível subir um novo processo, vamos atualizar nosso playbook adicionando uma task para encerrar um processo `cpf_validator_api` caso exista algum já iniciado. Vamos adicionar a task abaixo no nosso playbook, antes da inicialização do processo:

```
- name: encerrar processo
  command: pm2 delete cpf_validator_api
  ignore_errors: True
```

Isso deve resolver, tente executar o playbook novamente!
